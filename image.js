var http = require('http'),
    fs = require('fs'),
    images = require('images'),
    config = require("./config"),
    Image = require("./models/image")

var call = function (image, user, cb) {
    var Canvas = require('canvas')
        , Image = Canvas.Image
        , canvas = new Canvas(1181, 1772)
        , ctx = canvas.getContext('2d')
        , fs = require('fs')
        , opentype = require('opentype')
        , userpicName = user.username + '-avatar.jpg'

    fs.readFile(__dirname + '/public/images/border/' + image.hashtag + '.jpg', function (err, squid) {
        if (err) throw err;
        var img = new Image;
        img.src = squid;
        ctx.drawImage(img, 0, 0, img.width, img.height);

        var img = new Image;
        img.src = fs.readFileSync(__dirname + '/' + config.images.srcPrefix + userpicName)
//        ctx.drawImage(img, 49, 56, 109, 109);


        fs.readFile(__dirname + '/' + config.images.srcPrefix + image.filename, function (err, squid) {

            if (err) throw err;
            var img = new Image;
            img.src = squid;
            ctx.drawImage(img, 49, 200, 1080, 1080);

            opentype.load('public/fonts/adineue.otf', function (err, font) {
                if (err)
                    throw err

                // If you just want to draw the text you can also use font.draw(ctx, text, x, y, fontSize).
//                font.draw(ctx, user.username, 192, 124, 44, {fill: "black"});

                var out = fs.createWriteStream(__dirname + '/' + config.images.outPrefix + image.pathDatePrefix + image.filename);
                var stream = canvas.createPNGStream();

                stream.on('data', function (chunk) {
                    out.write(chunk);
                });

                stream.on('end', function () {
                    console.log('File with border created')
                    image.save(function (err) {
                        if (err) {
                            errors.push(err);
                            /**
                             * TODO: Error logging
                             */
                        } else {
                        }
                    })
                    if (cb)
                        cb(image)
                })
            })
        })
    })
};

var borderise = function (image, user, cb) {

    fs.mkdir(config.images.outPrefix + image.pathDatePrefix, function () {

        var userpicName = user.username + '-avatar.jpg';
        fs.exists(userpicName, function (exists) {
            if (!exists) {
                http.get(user.profile_picture, function (res) {
                    var imagedata = '';
                    res.setEncoding('binary')

                    res.on('data', function (chunk) {
                        imagedata += chunk;
                    })

                    res.on('end', function () {
                        fs.writeFile(__dirname + '/' + config.images.srcPrefix + userpicName, imagedata, 'binary', function (err) {
                            if (err) throw err;
                            call(image, user, cb);
                        })
                    })
                })
            } else {
                call(image, user, cb);
            }

        });

    })
};

module.exports = function (media, opts, cb) {
    var url = media.images.standard_resolution.url;
    http.get(url, function (res) {

        var imagedata = '',
            ext = url.substr(url.lastIndexOf('.')),
            filename = media.created_time + "_" + media.user.username.replace(/\s/g, '_') + ext,
            errors = [];


        res.setEncoding('binary')

        res.on('data', function (chunk) {
            imagedata += chunk
        })

        res.on('error', function (err) {
            console.log("Image download from instagram occured error:", err);
            errors.push(err);
        })

        res.on('end', function () {
            if (errors.length) {
                console.log(errors)
                return;
            }
            fs.writeFile(config.images.srcPrefix + filename, imagedata, 'binary', function (err) {
                if (err) throw err
                var i = {
                    "id": media.id,
                    "url": media.images.standard_resolution.url,
                    "hashtag": opts.tag,
                    "user": media.user.full_name,
                    "instagram": media.user.username.replace(/\s/g, '_'),
                    "page_id": opts.min_tag_id,
                    "filename": filename
                }
                if (!config.front.moderation)
                    i.moderationStatus = 2
                var image = new Image(i);
                borderise(image, media.user, cb);
            })
        })
    })
}