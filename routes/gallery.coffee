config = require('../config')
Image = require('../models/image');

module.exports = (req, res) ->
  yesterday = Date.now() - 60 * 60 * 24 * 1000
  Image.find
    dateTimePosted: { $gte:(new Date(yesterday))}
    , (err, images) ->
      throw err if err
      res.render "gallery",
        images: images