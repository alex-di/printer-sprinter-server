window.onload = function () {
// Создаем соединение с сервером; websockets почему-то в Хроме не работают, используем xhr
    window.config = {};


    $('#add-form form').submit(function () {

        var tagNode = $(this).find('[name="tag"]'),
            addTagNode = $(this).find('[name="additionalTag"]'),
            submitNode = $(this).find("[type='submit']"),
            fileNode = $(this).find('[type="file"]'),
            tag = tagNode.val().trim();

        if (tag !== '') {
            tagNode.prop('disabled', true);
            addTagNode.prop('disabled', true);
            submitNode.prop('disabled', true);
            var file = fileNode[0].files[0],
                stream = ss.createStream(socket)
            // upload a file to the server.

            if (file.type != 'image/jpeg') {
                alert('JPEG Н-Н-Н-НАДА!')
                return;
            }
            file.nname = tag + '.jpg';

            ss(socket).emit('image', stream, file);
            var streamData = ss.createBlobReadStream(file);

            streamData.on('end', function () {
                fileNode.val('')
                var opts = {
                    tag: tag,
                    additional: addTagNode.val().trim()
                }

                socket.emit("newTag", opts);
            })

            streamData.pipe(stream)
        }
        return false;
    })

    $("body").on("click", "a.init", function () {
        console.log("Init access")
        socket.emit("initTag", {object_id: $(this).data('subscription')})
        return false;
    })

    $("body").on("click", "a.clear", function () {
        if (confirm("Вы уверены, что хотите очистить базу данных?")) {
            socket.emit("clearDb");
            $(".overlay").addClass("loading");
        }
    })

    $('body').on('click', 'a.delete', function () {
        var sid = $(this).data('subscription');
        $(this).hide();
        $(this).parent().css('color', '#CCC')
        socket.emit("deleteTag", sid);
        return false;
    });


    $("body").on("click", 'a.add', function () {
        $(this).hide();
        $(this).next().show();
        return false;
    })
}
if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
    socket = io.connect('/', {'transports': ['xhr-polling']});
} else {
    socket = io.connect('/');
}

var clicking
socket.on('connect', function () {
    socket.on("config", function (config) {
        window.config = config;

        $("#limit .value").html(config.limit)
    })
    clicking = setInterval(function () {
        if(window.config.autoCall)
            $('.init').click();
    }, 10000)
    $(".overlay").animate({opacity: 0}, 500, function () {
        $(this).removeClass("loading").css('opacity', 1);
    })

    var reprint = function () {
        var id = $(this).parent().data('id');
        socket.emit('reprint', id);
    }

    socket.on('unprintedList', function (list) {
        list.forEach(function (image) {
            $(".toPrintList p[data-id='" + image._id + "']").remove()
            $('.toPrintList').append($('<p>', {'data-id': image._id, text: image.url + " (#" + image.hashtag + ")", class: (image.isPrinted ? 'printed' : '')}).append($('<a>').click(reprint)));
        })
    })

    socket.on("newTagAdded", function (data) {
        console.log("new tag response")
        var form = $('#add-form')
        form.hide();
        form.find('[name="tag"]').val('');
        form.prev().show();
        form.find('[name="tag"], [type="submit"]').prop('disabled', false);
        console.log(data)
        $('.subscriptions').append($('<p>', {html: data.tag + (data.additional ? "&nbsp;" + data.additional : "" ) + "&nbsp;" + "<a href='#' class='init' data-subscription='" + data.tag + "'>init</a>" + "<a href='/images/border/" + data.tag + ".jpg' target='_blank'>border</a> " + "<a href='#' class='delete' data-subscription='" + data.sid + "'>delete</a>"}))
    })

    socket.on('deletedTag', function (data) {
        $('[data-subscription="' + data + '"]').parent().remove();
    })

    socket.on("filePrinted", function (image) {
        $('.toPrintList [data-id="' + image._id + '"]').addClass('printed');
        if ($('.toPrintList p').length > 20)
            while ($('.toPrintList p') > 20 && $('.toPrintList p.printed') > 0)
                $('.toPrintList p.printed').first().remove();
    })

    socket.on("toModerateList", function (list) {
        list.forEach(function (image) {

            $(".toModerateList p[data-id='" + image._id + "']").remove()
            var url = "/images/dist/" + image.pathDatePrefix + image.filename;
            $('.toModerateList')
                .append($('<p>', {'data-id': image._id, text: ""})
                    .prepend(
                        $('<a>', {html: "<img src='" + image.url + "' width='150' height='150'/>", target: "_blank", href: url, style: "margin-right: 5px" })
                    )
                    .append(
                        $('<a>', {text: 'Печатать', href: '#', 'data-confirm': 'R u sure?', style: "color: green;"})
                            .click(function () {
                                socket.emit('fileApproved', image._id);
                                return false;
                            })
                    )
                    .append(
                        $('<a>', {text: 'Не печатать', href: "#", style: "color: red;"})
                            .click(
                            function () {
                                socket.emit('fileDenied', image._id);
                                return false;
                            })
                    )
                );
        })
    })

    socket.on("fileApproved", function (id) {
        $('p[data-id="' + id + '"]').remove();
    })
    socket.on("fileDenied", function (id) {
        $('p[data-id="' + id + '"]').remove();
    })

    socket.on("dbCleared", function () {
        $(".toModerateList *, .toPrintList *").remove();

        $(".overlay").animate({opacity: 0}, 500, function () {
            $(this).removeClass("loading").css('opacity', 1);
        })
    })


    socket.on('disconnect', function () {
        clearInterval(clicking)
        $(".overlay").addClass("loading")
    })

    socket.on("limitUpdate", function(limit){
        $("#limit .value").html(limit)
    })
})