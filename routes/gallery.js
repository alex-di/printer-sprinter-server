// Generated by CoffeeScript 1.7.1
(function() {
  var Image, config;

  config = require('../config');

  Image = require('../models/image');

  module.exports = function(req, res) {
    var yesterday;
    yesterday = Date.now() - 60 * 60 * 24 * 1000;
    return Image.find({
      dateTimePosted: {
        $gte: new Date(yesterday)
      }
    }, function(err, images) {
      if (err) {
        throw err;
      }
      return res.render("gallery", {
        images: images
      });
    });
  };

}).call(this);
