var mongoose = require('mongoose'),
    moment = require('moment'),
    Schema = mongoose.Schema,
    dt = moment().format('YYYY-MM-DD');

var Image = new Schema({
    "id": {type: String, index: true },
    "dateTimePosted": {type: Date, default: Date.now},
    "url": {type: String},
    "isPrinted": {type: Boolean, default: false},
    "hashtag": {type: String},
    "dateTimePrinted": {type: Date},
    "user": {type: String},
    "page_id": {type: String},
    "filename": {type: String },
    "pathDatePrefix": {type: String, default: dt + '/'},
    "moderationStatus": {type: Number, default: 0}
});

module.exports = mongoose.model('Image', Image);