###
Module dependencies.
###
express = require("express")
config = require("./config")
routes = require("./routes")
http = require("http")
path = require("path")
fs = require("fs")
mongoose = require("mongoose")
Image = require("./models/image.js")

global.config = config

#    Subscription = require('./models/subscriptions'),
ig = require("instagram-node").instagram()
media = require("./media").media
mediaProcessingBlock = false
SubscriptionScheme = new mongoose.Schema(
  tag: String
  additional: String
  sid: String
)
SubscriptionScheme.pre "save", (next) ->
  counter = 0
  self = this
  ig.add_tag_subscription @tag, config.instagram.listen, (err, result, limit) ->
    if err
      console.log err
      ++counter
      if counter < 5
        err.retry()
      else
        err = new Error("IG lost connection")
        next err
      return

    self.sid = result.id
    next()
    return

  return

SubscriptionScheme.pre "remove", (next) ->
  ig.del_subscription {id: this.sid}, ->
    next()


Subscription = mongoose.model("Subscription", SubscriptionScheme)

global.Subscription = Subscription

global.mediaProccessingBlock = mediaProcessingBlock
ig.use config.instagram
global.ig = ig
app = express()

# all environments
app.set "port", process.env.PORT or 4012
app.set "views", path.join(__dirname, "views")
app.set "view engine", "jade"
app.use express.favicon()
app.use express.logger("dev")
app.use express.json()
app.use express.urlencoded()
app.use express.methodOverride()
app.use app.router
app.use require("stylus").middleware(path.join(__dirname, "public"))
app.use express.static(path.join(__dirname, "public"))
mongoose.connect config.db

# development only
app.use express.errorHandler()  if "development" is app.get("env")
socketIO = require("socket.io")
socketIOstream = require("socket.io-stream")
global.io = socketIO
routes.add = require("./routes/add").add
routes.gallery = require("./routes/gallery")
auth = express.basicAuth((user, pass) ->
  user is "admin" and pass is "admTBWA"
)


app.get "/", auth, (req, res) ->
  tries = 0
  ig.subscriptions (err, result, limit) ->
    if err
      console.log "Subscriptions read err:", err
      ++tries
      if tries < 10
        err.retry()
      else
        res.render "index",
          title: "Subscriptions"
          subscriptions: []

    else
      console.log "Result at 91", result
    if(result)
      tags = for item in result
        item.object_id

      Subscription.remove { "tag": {$nin: tags}}
      .exec (err)->
        console.log err if err

      Subscription.find {}, (err, subs) ->
        res.render "index",
          title: "Subscriptions"
          subscriptions: subs

app.post "/listen", routes.add
app.get "/gallery", routes.gallery
app.get "/listen", (req, res) ->
  res.send 200, req.query["hub.challenge"]
  return

server = http.createServer(app).listen(app.get("port"), ->
  console.log "Express server listening on port " + app.get("port")
  return
)
io = socketIO.listen(server)
io.set "log level", 1
global.io = io
io.on "newImage", (data) ->
  console.log data
  result = new Array()
  result.push data.item
  call = (if (config.front.moderation) then "toModerateList" else "unprintedList")
  io.sockets.emit call, result
  return

io.on "reprint", (id, count) ->
  Image.find(_id: id).exec (err, data) ->
    console.log "Reprinted:", id, count, data
    throw err  if err
    i = 0

    while i < count
      io.sockets.emit "unprintedList", data
      i++
    return

  return

io.sockets.on "connection", (socket) ->
  console.log "Socket " + socket.id + " connected"
  socket.emit "config", config.front
  socketIOstream(socket).on "image", (stream, data) ->
    console.log "Socket stream:", data
    filename = path.basename(data.nname)
    stream.pipe fs.createWriteStream(config.images.borPrefix + filename)
    return

  Image.find(
    isPrinted: false
    moderationStatus: 2
  ).exec (err, data) ->
    socket.emit "unprintedList", data
    return

  Image.find(
    isPrinted: false
    moderationStatus:
      $nin: [
        1
        2
      ]
  ).exec (err, data) ->
    socket.emit "toModerateList", data
    return

  socket.on "newTag", (data) ->
    sub = new Subscription(data)
    sub.save (err, result) ->
      if err
        console.log err
        return
      io.sockets.emit "newTagAdded", result
      return

    return

  socket.on "deleteTag", (data) ->
    console.log data
    Subscription.findOne {sid: data}, (err, sub) ->
      console.log err, sub
      sub.remove ->
        console.log data
        io.sockets.emit "deletedTag", data


  socket.on "initTag", (tag) ->
    media tag, (limit) ->
      io.sockets.emit "limitUpdate", limit
      config.front.limit = limit
      return

    return

  socket.on "clearDb", ->
    i = new Image()
    i.collection.drop (err) ->
      throw err  if err
      console.log "collection dropped"
      io.sockets.emit "dbCleared"
      return

    return

  socket.on "filePrinted", (id) ->
    console.log("File Printed", id)
    Image.find(_id: id).exec (err, data) ->
      console.log "filePrinted"
      throw err  if err
      data[0].isPrinted = true
      data[0].save (err) ->
        throw err  if err
        io.sockets.emit "filePrinted", data[0]
        return

      return

    return

  socket.on "fileApproved", (id) ->
    Image.findOneAndUpdate
      _id: id
    ,
      $set:
        moderationStatus: 2
    , (err, data) ->
      throw err  if err
      io.sockets.emit "fileApproved", id
      result = new Array()
      result.push data
      io.sockets.emit "unprintedList", result
      return

    return

  socket.on "fileDenied", (id) ->
    Image.findOneAndUpdate
      _id: id
    ,
      $set:
        moderationStatus: 1
    , (err, data) ->
      throw err  if err
      io.sockets.emit "fileDenied", id
      return

    return

  socket.on "changeModeration", (val) ->
    global.config.front.moderation = val
    config.front.moderation = val

  return

io.on "error", (err) ->
  console.log "Caught flash policy server socket error: ", err.stack
  return

