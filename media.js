var Image = require('./models/image'),
//    Subscription = require("./models/subscriptions"),
    images = [],
    processMedia = require('./image')
ig = global.ig


var requestMedia = function (item, cb) {
    var opts = {};

    Subscription.findOne({tag: item.object_id}, function (err, sub) {

        Image.find({hashtag: sub.tag}).select('page_id').limit(1).sort({$natural: -1}).exec(function (err, data) {
            if (data.length)
                opts.min_tag_id = data[0].page_id
            console.log(item.object_id, opts);
            ig.tag_media_recent(item.object_id, opts, function (err, medias, pagination, limit) {
                console.log(limit)
                if (err) {
                    console.log("Instagram answers error:", err);
                    return;
                }
                console.log('Recent call ' + " (got " + medias.length + " images)");
                medias.forEach(function (media) {
                    if (sub.additional && media.tags.indexOf(sub.additional) == -1)
                        return;
                    var url = media.images.standard_resolution.url;

                    if (images.indexOf(url) > 0)
                        return;

                    processMedia(media, {tag: item.object_id, min_tag_id: pagination.min_tag_id}, function (image) {
                        global.io.emit('newImage', {item: image});
                        images.push(url);
                    });
                })
                if (typeof cb !== 'undefined')
                    cb(limit);
            });
        })
    })
}
var pool = {}

module.exports = {
    media: function (item, cb) {
        console.log("Instagram call")
        if (typeof pool[item.object_id] === "undefined" || pool[item.object_id] === null || !pool[item.object_id])
            pool[item.object_id] = {timeout: null, count: 0}

        if (pool[item.object_id].count < 15) {

            ++pool[item.object_id]
            clearTimeout(pool[item.object_id].timeout)
            pool[item.object_id].timeout = setTimeout(function () {
                requestMedia(item, cb)
            }, 30000)
        } else {
            requestMedia(item, cb)
            clearTimeout(pool[item.object_id].timeout)
        }

    }
}