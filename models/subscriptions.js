var mongoose = require('mongoose'),
    config = require('../config'),
    io = global.io,
    ig = global.ig,
    Schema = mongoose.Schema

var Subscription = new Schema({
    instId: {type: Number, index: true},
    active: {type: Boolean, default: false},
    tag: {type: String},
    border: {type: String},
    methods: {
        activate: function () {
            ig.add_tag_subscription(this.tag, config.instagram.listen, function (err, result, limit) {
                console.log(err, result);
                if (err)
                    throw err;
                io.sockets.emit("newTagAdded", result);
            });
        }
    }
})

module.exports = mongoose.model('Subscription', Subscription)